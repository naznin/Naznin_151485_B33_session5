
<?php
function test() {
    $foo = "local variable";

    echo '$foo in global scope: ' . $GLOBALS["foo"] . "</br>";//echos global foo
    echo '$foo in current scope: ' . $foo . ;//echos local foo
}

$foo = "Example content";
test();
?>
